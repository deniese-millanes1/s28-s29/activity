//First, we load the expressjs module to our application and saved it in a variable called EXPRESS
const express = require("express");

//Create an aplication with expressjs
	//This creates an application that uses express and stores in a variable called APP. Which makes it easier to use expressjs methods in our api
const app = express();

//port is just a variable to contain the port number we want ti designate for our new expressjs api
const port = 4000;

/*
	To create a new route in expressjs, we first access from our express() package, our method. For get method request, access express (app), and use get()
SYNTAX:
	app.routemethod('/',(req,res) => {
		//function to handle reqest and response
	})
*/

//
app.use(express.json());

app.get('/',(req,res) => {
	//res.send() ends the response and sends your response to the client
	//res.send() already does the res.writeHead() and res.end() automatically
	res.send("Hello World from our New ExpressJS API!");
}) 
app.get('/hello',(req,res) => {
	res.send("Hello, Batch 157!")
})
app.post("/hello",(req,res) => {
	console.log(req.body)
	res.send(`Hello I am ${req.body.name}, I am ${req.body.age}, I live in ${req.body.address}`)
})
let users = [];

app.post("/signup",(req,res) => {
	console.log(req.body)
	if(req.body.username != '' && req.body.password != '') {
		users.push(req.body);
		res.send(`User ${req.body.username} successfully registered.`);
	} else {
		res.send("Please input BOTH username and password");
	}
})
app.put("/change-password",(req,res) => {
	let message;
	
	for(let i=0; i<users.length; i++) {
		if(req.body.username === users[i].username) {
			users[i].password = req.body.password;
			message = `User ${req.body.username}'s password has been updated.`
			break;
		} else {
		message = `User does not exist`
		} 
	} 

	res.send(message)
})
//1. GET route /home and will print out a simple message
app.get("/home",(req,res) => {
	res.send("Welcome to the home page")
})
//3. GET /users and will retrieve all the users in the mock database.
app.get("/users",(req,res) => {
	res.send(users)
})
//5.DELETE /delete-user remove user from the mock database
app.delete("/delete-user",(req,res) => {
	let message;
	
	for(let i=0; i<users.length; i++) {
		if(req.body.username === users[i].username) {
			users[i].password = req.body.password;
			message = `User ${req.body.username} has been deleted	.`
			break;
		} else {
		message = `User does not exist`
		} 
	} 

	res.send(message)
})
//app.listen() allows us to designate the correct port to our new expressjs api and once the server is running we can then run a function that runs a console.log() which contains a message that the server is running 
app.listen(port,() => console.log(`Server is running at port ${port}`) );

/*
	Mini Activity
		Create a route in expressJS which will be able to send a message.
		endpoint:/hello
		message: Hello, Batch 157!
*/

/*
	Mini Activity
		Change the message that we send "Hello, I am <name>, I am <age>, I live in <address>"
*/